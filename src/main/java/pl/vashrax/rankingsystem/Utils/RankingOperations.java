package pl.vashrax.rankingsystem.Utils;

import lombok.NoArgsConstructor;
import pl.vashrax.rankingsystem.Data.Client;
import pl.vashrax.rankingsystem.Data.Movie;

import java.util.ArrayList;
import java.util.HashMap;

@NoArgsConstructor
public class RankingOperations {

    public double countClientSimilarityRate(Client clientX, Client clientY, ArrayList<Movie> movies){
        double result = 0;
        for(Movie movie : movies){
            if(clientX.hasShowMovie(movie) && clientY.hasShowMovie(movie)){
                result += (clientX.getMovieItem(movie).getPearsonCorelationFactor() * clientY.getMovieItem(movie).getPearsonCorelationFactor());
            }
        }
        return result;
    }

    public HashMap<Client, Double> getCustomerSimilarityMatrix(ArrayList<Client> clients, ArrayList<Movie> movies, int choosenClient){
        HashMap<Client, Double> cSM = new HashMap<>();
        for(Client client : clients){
            if(!client.equals(clients.get(choosenClient))){
                cSM.put(client, countClientSimilarityRate(client, clients.get(choosenClient), movies));
            }
        }
        return cSM;
    }

}
