package pl.vashrax.rankingsystem.App;

import lombok.NoArgsConstructor;
import pl.vashrax.rankingsystem.Data.Client;
import pl.vashrax.rankingsystem.Data.Movie;
import pl.vashrax.rankingsystem.Generator.DataGenerator;
import pl.vashrax.rankingsystem.Utils.RankingOperations;

import java.util.ArrayList;
import java.util.HashMap;

@NoArgsConstructor
public class Engine {

    private int choosenClient = 1;
    private int mostLikelyClientsCount = 2;
    private int recomendedProductsCount = 1;

    private DataGenerator dataGenerator = new DataGenerator();
    private RankingOperations rankingOperations = new RankingOperations();

    public void start(){

        choosenClient --; //Iteration from 0;

        ArrayList<Movie> movies = dataGenerator.createMovies();
        ArrayList<Client> clients = dataGenerator.createClients(movies);

        for(Client client : clients){
            System.out.println(client);
        }

        System.out.println("Customer Similarity Matrix : ");
        HashMap<Client, Double> customerSimilarityMatrix = rankingOperations.getCustomerSimilarityMatrix(clients,movies,choosenClient);
        customerSimilarityMatrix.forEach((k,v)->{
            System.out.println(k.getName() + " : " + v);
        });
    }

}
