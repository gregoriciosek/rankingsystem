package pl.vashrax.rankingsystem.Generator;

import lombok.NoArgsConstructor;
import pl.vashrax.rankingsystem.Data.Client;
import pl.vashrax.rankingsystem.Data.Movie;
import java.util.ArrayList;

/*
    HP1 HP2 HP3 ZM  GW1 GW2 GW3     sr
K1  4           5   1               10/3=3.33
K2  5   5   4                       14/3=4.66
K3              2   4   5           11/3=3.66
K4      3                   3       6/2=3
*/

@NoArgsConstructor
public class DataGenerator {

    public ArrayList<Movie> createMovies(){

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie("HP1"));
        movies.add(new Movie("HP2"));
        movies.add(new Movie("HP3"));
        movies.add(new Movie("ZM"));
        movies.add(new Movie("GW1"));
        movies.add(new Movie("GW2"));
        movies.add(new Movie("GW3"));

        return movies;
    }

    public ArrayList<Client> createClients(ArrayList<Movie> movies){

        ArrayList<Client> clients = new ArrayList<>();
        clients.add(new Client("K1"));
        clients.add(new Client("K2"));
        clients.add(new Client("K3"));
        clients.add(new Client("K4"));

        clients.get(0).addMovie(movies.get(0));
        clients.get(0).setRateToMovie(movies.get(0),4);
        clients.get(0).addMovie(movies.get(3));
        clients.get(0).setRateToMovie(movies.get(3),5);
        clients.get(0).addMovie(movies.get(4));
        clients.get(0).setRateToMovie(movies.get(4),1);

        clients.get(1).addMovie(movies.get(0));
        clients.get(1).setRateToMovie(movies.get(0),5);
        clients.get(1).addMovie(movies.get(1));
        clients.get(1).setRateToMovie(movies.get(1),5);
        clients.get(1).addMovie(movies.get(2));
        clients.get(1).setRateToMovie(movies.get(2),4);

        clients.get(2).addMovie(movies.get(3));
        clients.get(2).setRateToMovie(movies.get(3),2);
        clients.get(2).addMovie(movies.get(4));
        clients.get(2).setRateToMovie(movies.get(4),4);
        clients.get(2).addMovie(movies.get(5));
        clients.get(2).setRateToMovie(movies.get(5),5);

        clients.get(3).addMovie(movies.get(1));
        clients.get(3).setRateToMovie(movies.get(1),3);
        clients.get(3).addMovie(movies.get(6));
        clients.get(3).setRateToMovie(movies.get(6),3);

        return clients;
    }

}
