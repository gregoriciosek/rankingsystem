package pl.vashrax.rankingsystem;

import pl.vashrax.rankingsystem.App.Engine;

public class Main {
    public static void main(String [] args){
        Engine engine = new Engine();
        engine.start();
    }
}
