package pl.vashrax.rankingsystem.Data;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Client {


    private String name;
    private double averageRate;
    private ArrayList<MovieItem> shownMovies;

    public Client(String name){
        this.name = name;
        this.shownMovies = new ArrayList<>();
    }

    public void addMovie(Movie movie){
        this.shownMovies.add(new MovieItem(movie));
    }

    public MovieItem getMovieItem(Movie movie){
        for(MovieItem movieItem : this.shownMovies){
            if(movieItem.getMovie().equals(movie)){
                return movieItem;
            }
        }
        return null;
    }

    public void setRateToMovie(Movie movie, double rate){
        for(MovieItem movieItem : this.shownMovies){
            if(movieItem.getMovie().equals(movie))
                if(movieItem.setRate(rate)) {
                    this.countAverageRate();
                    countPearsonCorelationFactorToAllMovies();
                }

        }
    }

    public void countAverageRate(){
        double result = 0;
        for(MovieItem movieItem : shownMovies){
            result += movieItem.getRate();
        }
        this.averageRate = result / shownMovies.size();
    }

    public void countPearsonCorelationFactorToAllMovies(){
        for(MovieItem movieItem : this.shownMovies){
            movieItem.countPearsonCorelationFactor(averageRate);
        }
    }

    public boolean hasShowMovie(Movie movie){
        for(MovieItem movieItem : shownMovies){
            if(movieItem.getMovie().equals(movie))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", averageRate=" + averageRate +
                ", shownMovies=" + shownMovies +
                '}';
    }



}
