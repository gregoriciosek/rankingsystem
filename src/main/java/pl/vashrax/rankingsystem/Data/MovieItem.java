package pl.vashrax.rankingsystem.Data;


import lombok.Data;

@Data
public class MovieItem {

    private Movie movie;
    private double rate;
    private double pearsonCorelationFactor;

    public MovieItem(Movie movie){
        this.movie = movie;
    }

    public boolean setRate(double rate) {
        this.rate = rate;
        return true;
    }

    public void countPearsonCorelationFactor(double averageRate){
        this.pearsonCorelationFactor = this.rate - averageRate;

    }

    @Override
    public String toString() {
        return "MovieItem{" +
                "movie=" + movie +
                ", rate=" + rate +
                ", pearsonCorelationFactor=" + pearsonCorelationFactor +
                '}';
    }
}
